import React, { Component } from 'react'
import { connect } from 'react-redux';
import { ADD_TO_CART, CHANGE_DETAIL } from './redux/reducer/ReducerConstant';

class ItemShoe extends Component { 
  render() {
    let {name,image} = this.props.data;
    return (
      <div className='col-3 p-1'>
  <div className="card text-left h-100">
    <img className="card-img-top" src={image} alt />
    <div className="card-body">
      <h4 className="card-title">{name}</h4>
      <div>
        <button onClick={() => this.props.handleAddToCart(this.props.data)} className='btn btn-success mr-2'>Buy</button>
        <button onClick={()=>this.props.handleChangDetail(this.props.data)} className='btn btn-danger'>Detail</button>
      </div>
    </div>
  </div>
</div>

    )
  }
}
let mapDispatchToProps = (dispatch) => {
    return {
        handleChangDetail : (shoe) => {
            let action = {
                type: CHANGE_DETAIL,
                payload: shoe,
            }
            dispatch(action);
        },
        handleAddToCart : (shoe) => {
            let action = {
                type: ADD_TO_CART,
                payload: shoe,
            }
            dispatch(action);
        }
    }
}
export default connect(null,mapDispatchToProps)(ItemShoe)