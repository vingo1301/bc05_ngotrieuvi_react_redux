import React, { Component } from 'react'
import { connect } from 'react-redux'
import { GIAM_SO_LUONG, TANG_SO_LUONG } from './redux/reducer/ReducerConstant'

 class Cart extends Component {
    renderTbody = () => {
        return this.props.gioHang.map((item) => {
            return <tr>
                <td>{item.id}</td>
                <td>{item.name}</td>
                <td>{item.price * item.number}</td>
                <td>
                  <button onClick={()=> this.props.handleGiamSoLuong(item)} className='btn btn-warning'>-</button>
                  {item.number}
                  <button onClick={()=> this.props.handleTangSoLuong(item)} className='btn btn-success'>+</button>
                </td>
                <td>
                    <img style={{width:"80px"}} src={item.image} alt="" />
                </td>
                </tr>
        })

    }
  render() {
    return (
      <div>
        <table class="table">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Price</th>
                    <th>Quantity</th>
                    <th>Picture</th>
                </tr>
            </thead>
            <tbody>
                {this.renderTbody()}
            </tbody>
        </table>
      </div>
    )
  }
}
let mapStateToProps = (state) => {
    return{
        gioHang: state.shoeReducer.cart,
    }
}
let mapDispatchToProps = (dispatch) => {
    return{
        handleTangSoLuong: (shoe) => {
            let action = {
                type: TANG_SO_LUONG,
                payload: shoe,
            }
            dispatch(action);
        },
        handleGiamSoLuong: (shoe) => {
            let action = {
                type: GIAM_SO_LUONG,
                payload: shoe,
            }
            dispatch(action);
        }
    }
}
export default connect(mapStateToProps,mapDispatchToProps)(Cart)
