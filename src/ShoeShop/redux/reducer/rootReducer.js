import { shoeReducer } from "./ShoeReducer";
import { combineReducers} from "redux";

export const rootReducer = combineReducers({
    shoeReducer,
})