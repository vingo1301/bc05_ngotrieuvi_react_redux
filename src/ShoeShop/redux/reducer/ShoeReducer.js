import { dataShoe } from "../../dataShoe";
import { ADD_TO_CART, CHANGE_DETAIL, GIAM_SO_LUONG, TANG_SO_LUONG } from "./ReducerConstant";

let initialState = {
    shoeArr : dataShoe,
    detail: dataShoe[1],
    cart: [],
}
export const shoeReducer = (state = initialState,action) => {
    switch (action.type){
        case CHANGE_DETAIL:{
            state.detail = action.payload;
            return {...state};
        }
        case ADD_TO_CART:{
            let cloneCart = [...state.cart];
            let index = cloneCart.findIndex((item) => {
                return item.id == action.payload.id;
              });
              if (index == -1){
                // th1: sp chua co trong gio hang
                let cartItem = {...action.payload,number:1};
                cloneCart.push(cartItem);
              }
              else {
                // th2: da co.
                cloneCart[index].number++;
              }
            return {...state,cart: cloneCart}
        }
        case TANG_SO_LUONG:{
            let cloneCart = [...state.cart];
            let index = cloneCart.findIndex((item) => {
                return item.id == action.payload.id;
              });
            cloneCart[index].number++;
            return{...state,cart:cloneCart}
        }
        case GIAM_SO_LUONG: {
            let cloneCart = [...state.cart];
            let index = cloneCart.findIndex((item) => {
                return item.id == action.payload.id;
              });
            cloneCart[index].number--;
            if(cloneCart[index].number<=0){
                cloneCart.splice(index,1);
            }
            return{...state,cart:cloneCart}
        }
        default: return state;
    }
}