import React, { Component } from 'react'
import { connect } from 'react-redux'

class DetailShoe
 extends Component {
  render() {
    let shoe = this.props.item;
    return (
      <div className='row detailShoe'>
        <div className='col-6'>
            <img src={shoe.image} alt="" />
        </div>
        <div className='col-6 textDetail'>
            <h4>{shoe.name}</h4>
            <span>{shoe.price}</span>
            <p>{shoe.description}</p>
        </div>
      </div>
    )
  }
}
let mapStateToProps = (state) => {
    return{
        item: state.shoeReducer.detail,
    }
}
export default connect(mapStateToProps)(DetailShoe);
